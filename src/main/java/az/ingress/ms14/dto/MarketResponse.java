package az.ingress.ms14.dto;

import java.util.ArrayList;
import java.util.List;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class MarketResponse {

    Long id;
    String name;
    String type;
    List<BranchResponse> branches = new ArrayList<>();
    List<RegionResponse> regions=new ArrayList<>();

}
