package az.ingress.ms14.dto;

//import az.ingress.ms14.model.Address;
import az.ingress.ms14.model.Address;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class BranchResponse {

    Long id;
    String name;
    Integer countOfEmployee;

    Address address;
}
