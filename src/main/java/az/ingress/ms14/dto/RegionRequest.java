package az.ingress.ms14.dto;

//import az.ingress.ms14.model.RegionType;
import az.ingress.ms14.model.RegionType;
import jakarta.persistence.Column;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RegionRequest {

    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    RegionType name;
}
