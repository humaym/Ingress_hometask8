package az.ingress.ms14.repository;

import az.ingress.ms14.model.Address;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AddressRepository extends JpaRepository<Address, Long> {
}
