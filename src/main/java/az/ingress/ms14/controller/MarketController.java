package az.ingress.ms14.controller;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.service.MarketService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/market")
@RequiredArgsConstructor
public class MarketController {

    private final MarketService marketService;

    @PostMapping
    public MarketResponse create(@RequestBody @Valid MarketRequest request) {
        return marketService.create(request);
    }

    @PostMapping("/{marketId}/region/{regionId}")
        public MarketResponse addRegionToMarket(@PathVariable Long marketId, @PathVariable Long regionId){
           return marketService.addRegionToMarket(marketId,regionId);
        }

    @GetMapping("/{marketId}")
    public MarketResponse get(@PathVariable Long marketId) {
        return marketService.get(marketId);
    }

    @GetMapping
    public List<MarketResponse> getAll() {
        return marketService.getAll();
    }

    @DeleteMapping("/{marketId}")
    public void delete(@PathVariable Long marketId) {
        marketService.delete(marketId);
    }


    @PutMapping
    public MarketResponse update(@PathVariable Long marketId, @RequestBody @Valid MarketRequest request) {
        return marketService.update(marketId, request);
    }
}