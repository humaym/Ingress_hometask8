package az.ingress.ms14.controller;

import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/branch")
@RequiredArgsConstructor
public class BranchController {

    private final BranchService branchService;

    @PostMapping("/{marketId}")
    public BranchResponse create(@PathVariable Long marketId, @RequestBody BranchRequest request) {
        return branchService.create(marketId, request);
    }

    @PutMapping("/{marketId}/branch/{branchId}")
    public BranchResponse update(@PathVariable Long marketId,
                                 @PathVariable Long branchId,
                                 @RequestBody BranchRequest request) {
        return branchService.update(marketId, branchId, request);
    }

    @GetMapping("/{marketId}/branch/{branchId}")
    public BranchResponse get(@PathVariable Long branchId) {
     return branchService.get(branchId);
    }

    @GetMapping
    public List<BranchResponse> getAll(){
      return branchService.getAll();
    }

    @DeleteMapping("/{branchId}")
    public void delete(@PathVariable Long branchId){
        branchService.delete(branchId);
    }
}
