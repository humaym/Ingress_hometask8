package az.ingress.ms14.controller;


import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.service.RegionService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/region")
@RequiredArgsConstructor
public class RegionController {

    private final RegionService regionService;

    @PostMapping()
    public RegionResponse create(@RequestBody RegionRequest request) {
        return regionService.create(request);
    }

    @PutMapping("/{regionId}")
    public RegionResponse update(@PathVariable Long regionId, @RequestBody RegionRequest request) {
        return regionService.update(regionId, request);
    }

    @GetMapping("/{regionId}")
    public RegionResponse get(@PathVariable Long regionId) {
        return regionService.get(regionId);
    }

    @GetMapping
    public List<RegionResponse> getAll() {
        return regionService.getAll();
    }

    @DeleteMapping("/market/{marketId}/region/{regionId}")
            public void delete(@PathVariable Long marketId, @PathVariable Long regionId) {
        regionService.delete(marketId, regionId);
    }
}
