package az.ingress.ms14.controller;

import az.ingress.ms14.dto.AddressRequest;
import az.ingress.ms14.dto.AddressResponse;
import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.service.AddressService;
import az.ingress.ms14.service.BranchService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import org.w3c.dom.stylesheets.LinkStyle;

import java.util.List;

@RestController
@RequestMapping("/address")
@RequiredArgsConstructor
public class AddressController {

    private final AddressService addressService;

    @PostMapping("/{branchId}")
    public AddressResponse create(@PathVariable Long branchId, @RequestBody AddressRequest request) {
        return addressService.create(branchId, request);
    }

    @PutMapping("/{addressId}")
    public AddressResponse update(@PathVariable Long addressId,
                                  @RequestBody AddressRequest request) {
        return addressService.update(addressId, request);
    }

    @GetMapping("{addressId}")
    public AddressResponse get(@PathVariable Long addressId) {
        return addressService.get(addressId);
    }

    @GetMapping
    public List<AddressResponse> getAll(){
       return addressService.getAll();
    }

    @DeleteMapping("/{branchId}/address/{addressId}")
    public void delete(@PathVariable Long branchId, @PathVariable Long addressId){
        addressService.delete(branchId,addressId);
    }
}
