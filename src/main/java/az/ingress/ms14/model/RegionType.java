package az.ingress.ms14.model;

public enum RegionType {
    ABSHERON,
    MERKEZ,
    SUMQAYIT
}
