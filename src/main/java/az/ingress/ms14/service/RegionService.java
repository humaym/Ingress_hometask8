package az.ingress.ms14.service;

import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.dto.RegionRequest;
import az.ingress.ms14.dto.RegionResponse;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.MarketRepository;
import az.ingress.ms14.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class RegionService {

    private final RegionRepository regionRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;

    public RegionResponse create(RegionRequest request) {
        Region region = modelMapper.map(request, Region.class);
        regionRepository.save(region);
        return modelMapper.map(region, RegionResponse.class);
    }

    public RegionResponse update(Long regionId, RegionRequest request) {
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id %s not found", regionId)));
        region.setName(request.getName());
        regionRepository.save(region);
        return modelMapper.map(region, RegionResponse.class);
    }

    public void delete(Long marketId, Long regionId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id %s not found", regionId)));
        if (!market.getRegions().contains(region)) {
            throw new RuntimeException("Region not associated with Market");
        }
        market.getRegions().remove(region);
        marketRepository.save(market);
        regionRepository.delete(region);
    }

    public RegionResponse get(Long regionId){
        Region region=regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id %s not found", regionId)));
        return modelMapper.map(region,RegionResponse.class);
    }

    public List<RegionResponse> getAll() {
        List<Region> regions = regionRepository.findAll();
        List<RegionResponse> regionResponses = regions.stream()
                .map(region -> modelMapper.map(region, RegionResponse.class))
                .collect(Collectors.toList());
        return regionResponses;
    }
}
