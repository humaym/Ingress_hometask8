package az.ingress.ms14.service;


import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.repository.BranchRepository;
import az.ingress.ms14.repository.MarketRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BranchService {

    private final BranchRepository branchRepository;
    private final MarketRepository marketRepository;
    private final ModelMapper modelMapper;


    public BranchResponse create(Long marketId, BranchRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Branch branch = modelMapper.map(request, Branch.class);
        branch.setMarket(market);
        market.getBranches().add(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }


    public BranchResponse update(Long marketId, Long branchId, BranchRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        market.getBranches().remove(branch);
        branch.setName(request.getName());
        branch.setCountOfEmployee(request.getCountOfEmployee());
        market.getBranches().add(branch);
        marketRepository.save(market);
        return modelMapper.map(branch, BranchResponse.class);
    }


    @Transactional
    public BranchResponse get(Long branchId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        return modelMapper.map(branch, BranchResponse.class);
    }

    public List<BranchResponse> getAll() {
        List<Branch> branches = branchRepository.findAll();

        List<BranchResponse> branchResponses = branches.stream()
                .map(branch -> modelMapper.map(branch, BranchResponse.class))
                .collect(Collectors.toList());
        return branchResponses;
    }

    public void delete(Long branchId) {
        branchRepository.deleteById(branchId);
    }

}
