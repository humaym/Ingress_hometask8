package az.ingress.ms14.service;

import az.ingress.ms14.dto.AddressRequest;
import az.ingress.ms14.dto.AddressResponse;
import az.ingress.ms14.dto.BranchRequest;
import az.ingress.ms14.dto.BranchResponse;
import az.ingress.ms14.model.Address;
import az.ingress.ms14.model.Branch;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.repository.AddressRepository;
import az.ingress.ms14.repository.BranchRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AddressService {

    private final AddressRepository addressRepository;
    private final BranchRepository branchRepository;
    private final ModelMapper modelMapper;

    public AddressResponse create(Long branchId, AddressRequest request) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        Address address = modelMapper.map(request, Address.class);
        branch.setAddress(address);
        branchRepository.save(branch);
        return modelMapper.map(address, AddressResponse.class);
    }

    public AddressResponse update(Long addressId, AddressRequest request) {
        Address address = addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", addressId)));
        address.setName(request.getName());
        addressRepository.save(address);
        return modelMapper.map(address, AddressResponse.class);
    }

    @Transactional
    public AddressResponse get(Long addressId) {
        Address address = addressRepository.findById(addressId)
                .orElseThrow(() -> new RuntimeException(String.format("Address with id %s not found", addressId)));
        return modelMapper.map(address, AddressResponse.class);
    }

    @Transactional
    public List<AddressResponse> getAll() {
        List<Address> addresses = addressRepository.findAll();
        List<AddressResponse> addressResponses = addresses.stream()
                .map(address -> modelMapper
                        .map(address, AddressResponse.class))
                .collect(Collectors.toList());
        return addressResponses;
    }


    public void delete(Long branchId, Long addressId) {
        Branch branch = branchRepository.findById(branchId)
                .orElseThrow(() -> new RuntimeException(String.format("Branch with id %s not found", branchId)));
        branch.setAddress(null);
        addressRepository.deleteById(addressId);
    }
}
