package az.ingress.ms14.service;

import az.ingress.ms14.dto.MarketRequest;
import az.ingress.ms14.dto.MarketResponse;
import az.ingress.ms14.model.Market;
import az.ingress.ms14.model.Region;
import az.ingress.ms14.repository.MarketRepository;
import az.ingress.ms14.repository.RegionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MarketService {

    private final MarketRepository marketRepository;
    private final RegionRepository regionRepository;
    private final ModelMapper modelMapper;

    public MarketResponse create(MarketRequest request) {
        Market market = modelMapper.map(request, Market.class);
        market = marketRepository.save(market);
        return modelMapper.map(market, MarketResponse.class);
    }

    @Transactional
    public MarketResponse get(Long marketId) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        return modelMapper.map(market, MarketResponse.class);
    }

    public List<MarketResponse> getAll() {
        List<Market> markets = marketRepository.findAll();

        List<MarketResponse> marketResponses = markets.stream()
                .map(market -> modelMapper.map(market, MarketResponse.class))
                .collect(Collectors.toList());
        return marketResponses;
    }

    public void delete(Long marketId) {
        marketRepository.deleteById(marketId);
    }

    public MarketResponse update(Long marketId,MarketRequest request) {
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
            market.setName(request.getName());
            market.setType(request.getType());
            marketRepository.save(market);
        return modelMapper.map(request,MarketResponse.class);
    }

    public MarketResponse addRegionToMarket(Long marketId, Long regionId){
        Market market = marketRepository.findById(marketId)
                .orElseThrow(() -> new RuntimeException(String.format("Market with id %s not found", marketId)));
        Region region = regionRepository.findById(regionId)
                .orElseThrow(() -> new RuntimeException(String.format("Region with id %s not found", regionId)));
        market.getRegions().add(region);
        marketRepository.save(market);
        return modelMapper.map(market,MarketResponse.class);
    }
}
